FROM maven:3.5-jdk-8 AS build
COPY src /home/app/src
COPY pom.xml /home/app 
RUN mvn -f /home/app/pom.xml clean install

FROM tomcat
USER root
COPY --from=build /home/app/target/HelloWorld.war /usr/local/tomcat/webapps/HelloWorld.war
RUN chmod -R 777 /usr/local/tomcat/webapps
EXPOSE 8080
